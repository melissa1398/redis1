<?php
$redis = new Redis();
$conn = $redis->connect('127.0.0.1', 6379);

if ($conn) {
    echo "Conexion exitosa: estas conectado a Redis...</br>";
    $command = true;
}
else {
    echo "No se pudo conectar con la base de datos de Redis :(";
}

if ($command) {
    //Creamos un arreglo
    $redis->lpush("lista-bd", "xopa");
    $redis->lpush("lista-bd", "mopri");
    $redis->lpush("lista-bd", "palo");
    $redis->lpush("lista-bd", "vaina");
    $redis->lpush("lista-bd", "pelao");
    //Asignamos el arreglo a una variable
    $listbd = $redis->lrange("lista-bd", 0, 3);
    //Ciclo que muestra cada valor del arreglo
    foreach ($listbd as $nombre) {
        echo "<br>" . $nombre;
    }
}